﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public Form1(double xm, double y, double v)
        {
            //InitializeComponent();
            elementsVisible();
            Form1_Resize(this, null);
            DrawChart(Convert.ToDouble(xm), Convert.ToDouble(y), Convert.ToDouble(v));
        }
        public void F(double xm, double y, double v)
        {
            //InitializeComponent();
            elementsVisible();
            Form1_Resize(this, null);
            DrawChart(Convert.ToDouble(xm), Convert.ToDouble(y), Convert.ToDouble(v));
        }

        private void elementsVisible()
        {
            label1.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
            button1.Visible = false;
            button2.Visible = false;
            button3.Visible = false;
            textBox1.Visible = false;
            textBox2.Visible = false;
            comboBox1.Visible = false;
        }

        private void Form1_Resize(object sender, System.EventArgs e)
        {
            Control control = (Control)sender;
            if (control.Size.Height != control.Size.Width)
            {
                control.Size = new Size(chart1.Width + 40, chart1.Height + 40);
            }
        }
        public void DrawChart(double xm, double y, double v)
        {
            chart1.ChartAreas[0].AxisX.Minimum = 0; chart1.ChartAreas[0].AxisX.Maximum = 15000;
            double cm, cx, cy, s1, s2; int i = 1;
            cm = 0.073;
            this.chart1.Series[0].Points.Clear();
            this.chart1.Series[1].Points.Clear();
            
            s1 = 1;
            while (i < 15000)
            {
                if (i / xm <= 1) { s1 = 3 * Math.Pow(i / xm, 4) - 8 * Math.Pow(i / xm, 3) + 6 * Math.Pow(i / xm, 3); }
                if ((i / xm <= 8) || (i / xm > 1)) { s1 = 1.13 / (0.13 * Math.Pow(i / xm, 2) + 1); }
                if (i / xm > 8) { s1 = Math.Pow((0.1 * Math.Pow(i / xm, 2) + 2.47 * (i / xm) - 17.8), -1); }
                s2 = Math.Pow((1 + 8.4 * v * Math.Pow(y / i, 2)) * (1 + 28.2 * v * v * Math.Pow(y / i, 4)), -1);
                cx = 0.073 * s1;
                cy = cx * s2;
                this.chart1.Series[0].Points.AddXY(i, cx);
                this.chart1.Series[1].Points.AddXY(i, cy); i = i + 100;
            }
        }
        public void drawDefault()
        {
            string x, y, v;
            x = "1000";
            y = "200";
            v = "4";
            DrawChart(Convert.ToDouble(x), Convert.ToDouble(y), Convert.ToDouble(v));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                double v, xm, y;
                string str;
                str = Convert.ToString(textBox1.Text);
                xm = Convert.ToDouble(str);
                str = Convert.ToString(textBox2.Text);
                y = Convert.ToDouble(str);
                str = Convert.ToString(comboBox1.Text);
                v = Convert.ToDouble(str);
                DrawChart(xm, y, v);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //private void Label5_Paint(object sender, PaintEventArgs e)
        //{
        //    e.Graphics.Clear(this.BackColor);
        //    e.Graphics.RotateTransform(-90);
        //    SizeF textSize = e.Graphics.MeasureString(label5.Text, label5.Font);
        //    label5.Width = (int)textSize.Height + 2;
        //    label5.Height = (int)textSize.Width + 2;
        //    e.Graphics.TranslateTransform(-label5.Height / 2, label5.Width / 2);
        //    e.Graphics.DrawString(label5.Text, label5.Font, Brushes.Black, -(textSize.Width / 2), -(textSize.Height / 2));
        //}

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {           
            textBox1.Text = "1000";
            textBox2.Text = "200";
            comboBox1.Text = "4";
            drawDefault();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
