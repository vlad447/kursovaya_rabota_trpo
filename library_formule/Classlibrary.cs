﻿using System;

namespace LibPollutionformules
{

    public class AirPollutionCountClass
    {
        public double Velocity;
        public int E;

        //массив расстояний по Х
        public double[] X = new double[5] { 1000, 3000, 5000, 10000, 15000 };
        //массив скоростей
        public double[] u = new double[4] { 1, 2, 4, 6 };
        //массив расстояний по У
        public double[] Y = new double[5] { 100, 200, 300, 400, 500 };

        private double n;
        private double d;
        private double Xm;
        private double um;
        private double f;


        public double Wo_AverageVelocityOutfall(double V, double D)
        {
            return Math.Round(4 * (V / 3600) / (3.14 * Math.Pow(D, 2)), 4);
        }
        public double V1_MixtureVolume(double V, double D)
        {
            
            return Math.Round(3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4, 0);
        }

        public double M_DustAmount(double V, double D, double Zo)
        {
            
            return Math.Round(Zo * (3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4), 1);
        }
        public double f_Coefficient(double V, double D, double H, double Tr, double Ta)
        {
            
            return Math.Round((D * (Math.Pow((4 * (V / 3600) / (3.14 * Math.Pow(D, 2))), 2) * 1000)) / (Math.Pow(H, 2) * (Tr - Ta)), 3);
        }
        public double m_Coefficient(double V, double D, double H, double Tr, double Ta)
        {
            
            return Math.Round(Math.Pow(0.67 + 0.1 * Math.Sqrt(((D * (Math.Pow((4 * (V / 3600) / (3.14 * Math.Pow(D, 2))), 2) * 1000)) / (Math.Pow(H, 2) * (Tr - Ta)))) + 0.34 * Math.Sqrt(((D * (Math.Pow((4 * (V / 3600) / (3.14 * Math.Pow(D, 2))), 2) * 1000)) / (Math.Pow(H, 2) * (Tr - Ta)))), -1), 2);
        }
        public double Vm_Coefficient(double V, double D, double H, double Tr, double Ta)
        {
            
            return Math.Round((0.65 * Math.Pow((3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4) * (Tr - Ta) / H, 1.0 / 3.0)), 2);
        }
        public double n_Coefficient(double V, double D, double H, double Tr, double Ta)
        {
            if ((0.65 * Math.Pow(3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4 * (Tr - Ta) / H, 1.0 / 3.0)) <= 0.3)
            {
                n = 3;
            }
            if ((0.65 * Math.Pow(3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4 * (Tr - Ta) / H, 1.0 / 3.0)) > 0.3 & (0.65 * Math.Pow(3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4 * (Tr - Ta) / H, 1.0 / 3.0)) <= 2)
            {
                n = (3 - Math.Sqrt(((0.65 * Math.Pow(3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4 * (Tr - Ta) / H, 1.0 / 3.0)) - 0.3) * (4.36 - (0.65 * Math.Pow(3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4 * (Tr - Ta) / H, 1.0 / 3.0)))));
            }
            if ((0.65 * Math.Pow(3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4 * (Tr - Ta) / H, 1.0 / 3.0)) > 2)
            {
                n = 1;
            }
            return n;
        }
        public double F_Coefficient(double E, double nu)
        {
            if (E == 1)
            {
                f = 1;
            }
            if (E == 0 && nu >= 90)
            {
                f = 2;
            }
            if (E == 0 && nu < 75)
            {
                f = 3;
            }
            if (E == 0 && nu >= 75 && nu < 90)
            {
                f = 2.5;
            }
            return f;
        }

        public double Cm_MaxPollutionConcentration(double A, double V, double D, double Zo, double E, double nu, double Tr, double Ta, double H)
        {
            return Math.Round((A * (Zo * (3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4)) * F_Coefficient(E, nu) * (Math.Pow(0.67 + 0.1 * Math.Sqrt(((D * (Math.Pow((4 * (V / 3600) / (3.14 * Math.Pow(D, 2))), 2) * 1000)) / (Math.Pow(H, 2) * (Tr - Ta)))) + 0.34 * Math.Sqrt(((D * (Math.Pow((4 * (V / 3600) / (3.14 * Math.Pow(D, 2))), 2) * 1000)) / (Math.Pow(H, 2) * (Tr - Ta)))), -1)) * n_Coefficient(V, D, H, Tr, Ta)) / (Math.Pow(H, 2) * Math.Pow((3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4) * (Tr - Ta), 1.0 / 3.0)), 2);
        }




        public double d_Coefficient(double V, double D, double H, double Tr, double Ta, double Zo)
        {
            if (((0.65 * Math.Pow((3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4) * (Tr - Ta) / H, 1.0 / 3.0))) <= 2)
            {
                d = 4.95 * ((0.65 * Math.Pow((3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4) * (Tr - Ta) / H, 1.0 / 3.0))) * (1 + 0.28 * Math.Pow(((D * (Math.Pow((4 * (V / 3600) / (3.14 * Math.Pow(D, 2))), 2) * 1000)) / (Math.Pow(H, 2) * (Tr - Ta))), 1.0 / 3.0));
            }
            if (((0.65 * Math.Pow((3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4) * (Tr - Ta) / H, 1.0 / 3.0))) > 2)
            {
                d = 7 * Math.Sqrt(((0.65 * Math.Pow((3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4) * (Tr - Ta) / H, 1.0 / 3.0)))) * (1 + 0.28 * Math.Pow(((D * (Math.Pow((4 * (V / 3600) / (3.14 * Math.Pow(D, 2))), 2) * 1000)) / (Math.Pow(H, 2) * (Tr - Ta))), 1.0 / 3.0));
            }
            return Math.Round(d, 2);
        }
        public double Xm_TorchDistance(double E, double nu, double V, double D, double H, double Tr, double Ta, double Zo)
        {
            if (((0.65 * Math.Pow((3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4) * (Tr - Ta) / H, 1.0 / 3.0))) >= 2)
            {
                Xm = H * d_Coefficient(V, D, H, Tr, Ta, Zo) * ((5 - F_Coefficient(E, nu)) / 4);
            }
            if (((0.65 * Math.Pow((3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4) * (Tr - Ta) / H, 1.0 / 3.0))) < 2)
            {
                Xm = H * d_Coefficient(V, D, H, Tr, Ta, Zo);
            }
            return Xm;
        }



        public double um_DangerousWindVelocity(double V, double D, double H, double Tr, double Ta)
        {

            if (((0.65 * Math.Pow((3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4) * (Tr - Ta) / H, 1.0 / 3.0))) > 2)
            {
                um = Math.Round(((0.65 * Math.Pow((3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4) * (Tr - Ta) / H, 1.0 / 3.0))) * (1 + 0.12 * Math.Sqrt(((D * (Math.Pow((4 * (V / 3600) / (3.14 * Math.Pow(D, 2))), 2) * 1000)) / (Math.Pow(H, 2) * (Tr - Ta))))), 2);
            }
            if (((0.65 * Math.Pow((3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4) * (Tr - Ta) / H, 1.0 / 3.0))) > 0.5 & ((0.65 * Math.Pow((3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4) * (Tr - Ta) / H, 1.0 / 3.0))) <= 2)
            {
                um = ((0.65 * Math.Pow((3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4) * (Tr - Ta) / H, 1.0 / 3.0)));
            }
            if (((0.65 * Math.Pow((3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4) * (Tr - Ta) / H, 1.0 / 3.0))) <= 0.5)
            {
                um = Math.Round(0.5 * ((0.65 * Math.Pow((3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4) * (Tr - Ta) / H, 1.0 / 3.0))), 2);
            }
            return um;
        }



        public double[] r_CoefficientArray = new double[4];
        public double[] r_CoefficientCount(double V, double D, double H, double Tr, double Ta)
        {
            for (int i = 0; i < r_CoefficientArray.Length; i++)
            {
                if ((u[i] / um_DangerousWindVelocity(V, D, H, Tr, Ta)) < 1)
                {
                    r_CoefficientArray[i] = Math.Round(0.67 * (u[i] / um_DangerousWindVelocity(V, D, H, Tr, Ta)) + 1.67 * Math.Pow(u[i] / um_DangerousWindVelocity(V, D, H, Tr, Ta), 2) - 1.34 * Math.Pow(u[i] / um_DangerousWindVelocity(V, D, H, Tr, Ta), 3), 3);
                }
                if ((u[i] / um_DangerousWindVelocity(V, D, H, Tr, Ta)) > 1)
                {
                    r_CoefficientArray[i] = Math.Round(3 * (u[i] / um_DangerousWindVelocity(V, D, H, Tr, Ta)) / (2 * Math.Pow((u[i] / um_DangerousWindVelocity(V, D, H, Tr, Ta)), 2) - (u[i] / um_DangerousWindVelocity(V, D, H, Tr, Ta)) + 2), 3);
                }
            }
            return r_CoefficientArray;
        }



        public double[] Cmv_MaxPollutionConcetrationVelocityArray = new double[4];
        public double[] Cmv_MaxPolutionConcentrationVelocityCount(double V, double D, double H, double Tr, double Ta, double Zo, double A, double E, double nu)
        {
            for (int i = 0; i < Cmv_MaxPollutionConcetrationVelocityArray.Length; i++)
            {
                Cmv_MaxPollutionConcetrationVelocityArray[i] = Math.Round(r_CoefficientCount(V, D, H, Tr, Ta)[i] * ((A * (Zo * (3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4)) * F_Coefficient(E, nu) * (Math.Pow(0.67 + 0.1 * Math.Sqrt(((D * (Math.Pow((4 * (V / 3600) / (3.14 * Math.Pow(D, 2))), 2) * 1000)) / (Math.Pow(H, 2) * (Tr - Ta)))) + 0.34 * Math.Sqrt(((D * (Math.Pow((4 * (V / 3600) / (3.14 * Math.Pow(D, 2))), 2) * 1000)) / (Math.Pow(H, 2) * (Tr - Ta)))), -1)) * n_Coefficient(V, D, H, Tr, Ta)) / (Math.Pow(H, 2) * Math.Pow((3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4) * (Tr - Ta), 1.0 / 3.0))), 4);
            }
            return Cmv_MaxPollutionConcetrationVelocityArray;
        }



        public double[] Xmu_MaxConcentrationDistanceArray = new double[4];
        public double[] Xmu_MaxConcentrationDistanceCount(double E, double nu, double V, double D, double H, double Tr, double Ta, double Zo)
        {
            for (int i = 0; i < Xmu_MaxConcentrationDistanceArray.Length; i++)
            {
                if ((u[i] / um) < 0.25)
                {
                    Xmu_MaxConcentrationDistanceArray[i] = Math.Round(3 * Xm_TorchDistance(E, nu, V, D, H, Tr, Ta, Zo), 0);
                }
                if ((u[i] / um) < 1 && (u[i] / um) >= 0.25)
                {
                    Xmu_MaxConcentrationDistanceArray[i] = Math.Round((8.43 * Math.Pow(1 - (u[i] / um_DangerousWindVelocity(V, D, H, Tr, Ta)), 5) + 1) * Xm_TorchDistance(E, nu, V, D, H, Tr, Ta, Zo), 0);
                }
                if ((u[i] / um) > 1)
                {
                    Xmu_MaxConcentrationDistanceArray[i] = Math.Round((0.32 * (u[i] / um_DangerousWindVelocity(V, D, H, Tr, Ta)) + 0.68) * Xm_TorchDistance(E, nu, V, D, H, Tr, Ta, Zo), 0);
                }
            }
            return Xmu_MaxConcentrationDistanceArray;
        }
        public double[] S1_CoefficientArray = new double[5];
        public double[] S1_CoefficientCount(double E, double nu, double V, double D, double H, double Tr, double Ta, double Zo)
        {
            for (int i = 0; i < S1_CoefficientArray.Length; i++)
            {
                if ((X[i] / Xm_TorchDistance(E, nu, V, D, H, Tr, Ta, Zo)) <= 1)
                {
                    S1_CoefficientArray[i] = (3 * Math.Pow((X[i] / Xm_TorchDistance(E, nu, V, D, H, Tr, Ta, Zo)), 4) - 8 * Math.Pow((X[i] / Xm_TorchDistance(E, nu, V, D, H, Tr, Ta, Zo)), 3) + 6 * Math.Pow((X[i] / Xm_TorchDistance(E, nu, V, D, H, Tr, Ta, Zo)), 2));
                }
                if ((X[i] / Xm_TorchDistance(E, nu, V, D, H, Tr, Ta, Zo)) <= 8 && (X[i] / Xm_TorchDistance(E, nu, V, D, H, Tr, Ta, Zo)) > 1)
                {
                    S1_CoefficientArray[i] = (1.13 / (0.13 * Math.Pow((X[i] / Xm_TorchDistance(E, nu, V, D, H, Tr, Ta, Zo)), 2) + 1));
                }
                if ((X[i] / Xm_TorchDistance(E, nu, V, D, H, Tr, Ta, Zo)) > 8)
                {
                    S1_CoefficientArray[i] = (Math.Pow(0.1 * Math.Pow((X[i] / Xm_TorchDistance(E, nu, V, D, H, Tr, Ta, Zo)), 2) + 2.47 * (X[i] / Xm_TorchDistance(E, nu, V, D, H, Tr, Ta, Zo)) - 17.8, -1));
                }

            }
            return S1_CoefficientArray;
        }
        public double[] Cx_ConcentrationCountOfDistanceArray = new double[5];
        public double[] Cx_ConcentrationCountOfDistanceCount(double E, double nu, double V, double D, double H, double Tr, double Ta, double Zo, double A)
        {
            for (int i = 0; i < Cx_ConcentrationCountOfDistanceArray.Length; i++)
            {
                Cx_ConcentrationCountOfDistanceArray[i] = (S1_CoefficientCount(E, nu, V, D, H, Tr, Ta, Zo)[i] * ((A * (Zo * (3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4)) * F_Coefficient(E, nu) * (Math.Pow(0.67 + 0.1 * Math.Sqrt(((D * (Math.Pow((4 * (V / 3600) / (3.14 * Math.Pow(D, 2))), 2) * 1000)) / (Math.Pow(H, 2) * (Tr - Ta)))) + 0.34 * Math.Sqrt(((D * (Math.Pow((4 * (V / 3600) / (3.14 * Math.Pow(D, 2))), 2) * 1000)) / (Math.Pow(H, 2) * (Tr - Ta)))), -1)) * n_Coefficient(V, D, H, Tr, Ta)) / (Math.Pow(H, 2) * Math.Pow((3.14 * Math.Pow(D, 2) * (4 * (V / 3600) / (3.14 * Math.Pow(D, 2))) / 4) * (Tr - Ta), 1.0 / 3.0))));
            }
            return Cx_ConcentrationCountOfDistanceArray;
        }
        public double[,] S2_CoefficientArray = new double[5, 5];

        public double[,] S2_CoefficientCount(double Velocity)
        {
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    S2_CoefficientArray[i, j] = Math.Pow((1d + 8.4d * Velocity * Math.Pow((Y[i] / X[j]), 2)) * (1d + 28.2d * Math.Pow(u[0], 2) * Math.Pow((Y[i] / X[j]), 4)), -1);
                }
            }
            return S2_CoefficientArray;
        }

        public double[,] Cy_ConcentrationCountOfDistanceArray = new double[5, 5];
        public double[,] Cy_ConcentrationCountOfDistanceCount(double E, double nu, double V, double D, double H, double Tr, double Ta, double Zo, double A)
        {

            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Cy_ConcentrationCountOfDistanceArray[j, i] = (S2_CoefficientCount(Velocity)[i, j] * Cx_ConcentrationCountOfDistanceCount(E, nu, V, D, H, Tr, Ta, Zo, A)[j]);
                }
            }
            return Cy_ConcentrationCountOfDistanceArray;
        }

    }
}