﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LibPollutionformules;
using System;
namespace TestProject1
{
    [TestClass]
    public class AirPollTests
    {
        AirPollutionCountClass f1 = new AirPollutionCountClass();
        double H = 80;
        double D = 6.4;
        double V = 1198800;
        int Tr = 100;
        int Ta = 30;
        double Zo = 0.1;
        double nu = 75;

        double A = 160;

        [TestMethod]
        public void TestWo_AverageVelocityOutfall()
        {
            double result = f1.Wo_AverageVelocityOutfall(V, D);
            Assert.AreEqual(10.3565, result);
        }


        
        
        [TestMethod]
        public void TestV1_MixtureVolume()
        {
            AirPollutionCountClass f2 = new AirPollutionCountClass();
            double result = f2.V1_MixtureVolume(V, D);
            Assert.AreEqual(333, result);
        }

       
        
        [TestMethod]
        public void TestM_DustAmount()
        {
            AirPollutionCountClass f3 = new AirPollutionCountClass();
            double result = f3.M_DustAmount(V, D, Zo);
            Assert.AreEqual(33.3, result);
        }

       
        [TestMethod]
        public void TestFCoeff()
        {
            AirPollutionCountClass f4 = new AirPollutionCountClass();
            f4.Velocity = 1;
            f4.E = 0;
            double result = f4.f_Coefficient(V, D, H, Tr, Ta);
            Assert.AreEqual(1.53, Math.Round(result, 2));
        }

       
        [TestMethod]
        public void TestMCoeff()
        {

            AirPollutionCountClass f5 = new AirPollutionCountClass();
            f5.Velocity = 1;
            f5.E = 0;
            double result = f5.m_Coefficient(V, D, H, Tr, Ta);
            Assert.AreEqual(0.82, result);
        }

        [TestMethod]
        public void TestVm_Coefficient()
        {
            AirPollutionCountClass f6 = new AirPollutionCountClass();
            f6.Velocity = 1;
            f6.E = 0;
            double result = f6.Vm_Coefficient(V, D, H, Tr, Ta);
            Assert.AreEqual(4.31, result);
        }
        [TestMethod]
        public void Testn_Coefficient()
        {
            AirPollutionCountClass f7 = new AirPollutionCountClass();
            f7.Velocity = 1;
            f7.E = 0;
            double result = f7.n_Coefficient(V, D, H, Tr, Ta);
            Assert.AreEqual(1, result);
        }
        [TestMethod]
        public void TestF_Coefficient()
        {
            AirPollutionCountClass f8 = new AirPollutionCountClass();
            f8.Velocity = 1;
            f8.E = 0;
            double result = f8.F_Coefficient(f8.E, nu);
        }
        [TestMethod]
        public void TestCm_MaxPollutionConcentration()
        {
            AirPollutionCountClass f9 = new AirPollutionCountClass();
            f9.Velocity = 1;
            f9.E = 0;
            double result = f1.Cm_MaxPollutionConcentration(A, V, D, Zo, f9.E, nu, Tr, Ta, H);
            Assert.AreEqual(0.06, result);
        }
        [TestMethod]
        public void Testd_Coefficient()
        {
            AirPollutionCountClass f10 = new AirPollutionCountClass();
            f10.Velocity = 1;
            f10.E = 0;
            double result = f10.d_Coefficient(V, D, H, Tr, Ta, Zo);
            Assert.AreEqual(19.22, result);
        }
        [TestMethod]
        public void TestXm_TorchDistance()
        {
            AirPollutionCountClass f11 = new AirPollutionCountClass();
            f11.Velocity = 1;
            f11.E = 0;
            double result = f1.Xm_TorchDistance(f11.E, nu, V, D, H, Tr, Ta, Zo);
            Assert.AreEqual(961, result);
        }
        [TestMethod]
        public void Testum_DangerousWindVelocity()
        {
            AirPollutionCountClass f12 = new AirPollutionCountClass();
            f12.Velocity = 1;
            f12.E = 0;
            double result = f12.um_DangerousWindVelocity(V, D, H, Tr, Ta);
            Assert.AreEqual(4.95, result);
        }
        [TestMethod]
        public void Testr_CoefficientCount()
        {
            AirPollutionCountClass f13 = new AirPollutionCountClass();
            f13.Velocity = 1;
            f13.E = 0;
            double result = f13.r_CoefficientCount(V, D, H, Tr, Ta)[3];
            Assert.AreEqual(0.976, result);

        }
        [TestMethod]
        public void TestCmv_MaxPolutionConcentrationVelocityCount()
        {
            AirPollutionCountClass f14 = new AirPollutionCountClass();
            f14.Velocity = 1;
            f14.E = 0;
            double result = f14.Cmv_MaxPolutionConcentrationVelocityCount(V, D, H, Tr, Ta, Zo, A, f14.E, nu)[3];
            Assert.AreEqual(0.0585, result);
        }
        [TestMethod]
        public void TestXmu_MaxConcentrationDistanceCount()
        {
            AirPollutionCountClass f15 = new AirPollutionCountClass();
            f15.Velocity = 1;
            f15.E = 0;
            double result = f15.Xmu_MaxConcentrationDistanceCount(f15.E, nu, V, D, H, Tr, Ta, Zo)[3];
            Assert.AreEqual(1026, result);
        }
        [TestMethod]
        public void TestS1_CoefficientCount()
        {
            AirPollutionCountClass f16 = new AirPollutionCountClass();
            f16.Velocity = 1;
            f16.E = 0;
            double result = f16.S1_CoefficientCount(f16.E, nu, V, D, H, Tr, Ta, Zo)[4];
            Assert.AreEqual(0.022, Math.Round(result, 3));
        }
        [TestMethod]
        public void TestCx_ConcentrationCountOfDistanceCount()
        {
            AirPollutionCountClass f17 = new AirPollutionCountClass();
            f17.Velocity = 1;
            f17.E = 0;
            double result = f17.Cx_ConcentrationCountOfDistanceCount(f17.E, nu, V, D, H, Tr, Ta, Zo, A)[4];
            Assert.AreEqual(0.0013, Math.Round(result, 4));
        }
        [TestMethod]
        public void TestS2_CoefficientCount()
        {
            AirPollutionCountClass f18 = new AirPollutionCountClass();
            f18.Velocity = 1;
            f18.E = 0;
            double result = f18.S2_CoefficientCount(f18.Velocity)[4, 4];
            Assert.AreEqual(0.99, Math.Round(result, 2));
        }
        [TestMethod]
        public void TestCy_ConcentrationCountOfDistanceCount()
        {
            AirPollutionCountClass f19 = new AirPollutionCountClass();
            f19.Velocity = 1;
            f19.E = 0;
            double result = f19.Cy_ConcentrationCountOfDistanceCount(f19.E, nu, V, D, H, Tr, Ta, Zo, A)[4, 4];
            Assert.AreEqual(0.0013, Math.Round(result, 4));
        }
        [TestMethod]
        public void TestConcentrationCountOfDistanceCount()
        {
            AirPollutionCountClass f20 = new AirPollutionCountClass();
            int k = 0, j = 0;
            double E = f20.E;
            var result = Math.Round(f20.Cy_ConcentrationCountOfDistanceCount(Convert.ToDouble(E), Convert.ToDouble(nu), Convert.ToDouble(V), Convert.ToDouble(D), Convert.ToDouble(H), Convert.ToInt32(Tr), Convert.ToInt32(Ta), Convert.ToDouble(Zo), Convert.ToInt32(A))[k, j], 4);

            Assert.AreEqual(0.0592, result);
        }



        [TestMethod]
        public void TestDangerVelocity()
        {
            AirPollutionCountClass f21 = new AirPollutionCountClass();
            var result = f21.um_DangerousWindVelocity(Convert.ToDouble(V), Convert.ToDouble(D), Convert.ToDouble(H), Convert.ToInt32(Tr), Convert.ToInt32(Ta));
            Assert.AreEqual(4.95, result);
        }
    }
}
